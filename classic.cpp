#include <bits/stdc++.h>
#include <string>

using namespace std;

#include <random>
#include <chrono>

const size_t SIZE = 1000;
const size_t NUM_MATRICES = 10;
const int k = 8;
const float FILTER = std::log2(powf(0.25, k));


int binpow (int a, int n) {
	int res = 1;
	while (n) {
		if (n & 1)
			res *= a;
		a *= a;
		n >>= 1;
	}
	return res;
}

 
class RandomGenerator
{
public:
	static mt19937 & getMt19937();
 
private:
	RandomGenerator();
	~RandomGenerator() {}
	static RandomGenerator& instance();
 
	RandomGenerator(RandomGenerator const&) = delete;
	RandomGenerator& operator= (RandomGenerator const&) = delete;
 
	mt19937 mMt;
};
 
RandomGenerator::RandomGenerator() {
	random_device rd;
    mMt.seed(42);
}
 
RandomGenerator& RandomGenerator::instance() {
	static RandomGenerator s;
	return s;
}
 
mt19937 & RandomGenerator::getMt19937() {
	return RandomGenerator::instance().mMt;
}

vector <vector <float>> gen() {
	mt19937 &mt = RandomGenerator::getMt19937();
	uniform_real_distribution <float> dist(0.0, 1.0);
    
    vector <vector <float>> arr(SIZE, vector <float>(4));
    for (size_t i = 0; i < SIZE; i++) {
    	for (int j = 0; j < 4; j++) {
    		arr[i][j] = log2(dist(mt));
//    		cout << arr[i][j] << " ";
		}
//		cout << endl;
	}
	return arr;
}


unordered_map <int, float> f(vector <vector <float>> arr) {
    unordered_map <int, float> m;
	float ma, sum;
	int tek, iter;
	vector <unsigned short int> a(k);
	for (int i = 0; i < binpow(4, k); i++) {
		ma = -FLT_MAX;
		tek = i;
		iter = k - 1;
		while (tek > 0) {
			a[iter--] = (tek & 0b11);
			tek >>= 2;
		}
		for (size_t j = 0; j <= SIZE - k; j++) {
			sum = 0;
			for (int x = 0; x < k; x++) {
				sum += arr[j + x][a[x]];
			}
			ma = max(ma, sum);
		}
		if (ma > FILTER) {
			m[i] = ma;
		}
	}
//	for (const auto & p : m) {
//		cout << p.first << " : " << p.second << endl;
//	}
    return m;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    /// посчитаем, сколько ключей было добавлено во все мапы
    size_t total_num_keys = 0;

    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - std::chrono::steady_clock::now()).count();
    for (size_t i = 0; i < NUM_MATRICES; i++) {
        const auto matrix = gen();

        //print_matrix(matrix);
        auto begin = std::chrono::steady_clock::now();

        /// получаем мапу и запоминаем, сколько ключей в нее вошло
        const auto map = f(matrix);
        total_num_keys = map.size();

        auto end = std::chrono::steady_clock::now();
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ";
        time += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    }

    std::cout << std::endl;
    std::cout << "Total number of keys: " << total_num_keys << std::endl;
    std::cout << "Average mean time: " << (float)time / (float)NUM_MATRICES << " ms" << std::endl;
    return 0;
}