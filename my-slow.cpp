#include <unordered_map>
#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <map>


using namespace std;


const size_t SIZE = 1000;
const size_t NUM_MATRICES = 10;
const size_t k = 9;
const float FILTER = log2(pow(0.25, k));


int binpow (int a, int n) {
    int res = 1;
    while (n) {
        if (n & 1)
            res *= a;
        a *= a;
        n >>= 1;
    }
    return res;
}


class RandomGenerator
{
public:
    static mt19937 & getMt19937();

private:
    RandomGenerator();
    ~RandomGenerator() {}
    static RandomGenerator& instance();

    RandomGenerator(RandomGenerator const&) = delete;
    RandomGenerator& operator= (RandomGenerator const&) = delete;

    mt19937 mMt;
};

RandomGenerator::RandomGenerator() {
    random_device rd;
    mMt.seed(42);
}

RandomGenerator& RandomGenerator::instance() {
    static RandomGenerator s;
    return s;
}

mt19937 & RandomGenerator::getMt19937() {
    return RandomGenerator::instance().mMt;
}


vector <vector <float>> gen() {
    mt19937 &mt = RandomGenerator::getMt19937();
    uniform_real_distribution <float> dist(0.0, 1.0);

    vector <vector <float>> arr(SIZE, vector <float>(4));
    for (size_t i = 0; i < SIZE; i++) {
        for (size_t j = 0; j < 4; j++) {
            arr[i][j] = log2(dist(mt));
//    		cout << arr[i][j]  << " ";
        }
//		cout << endl;
    }
    return arr;
}

unordered_map <int, float> f(int k, vector <vector <float>> arr) {

    unordered_map <int, float> m;

    float sum;
    int tek, iter, dat, step;
    vector <unsigned short int> a(k, 0), num(k);

    for (int i = 0; i < binpow(4, k); i++) {
        tek = i;
        iter = k - 1;
        while (tek > 0) {
            a[iter--] = (tek & 0b11);
            tek >>= 2;
        }
        for (int j = 0; j < k; j++) {
            dat = 0;
            step = 1;
            for (int x = k - 1; x >= 0; x--) {
                dat += step * a[x];
                step *= 4;
            }
            num[j] = dat;
            for (int x = 0; x < k - 1; x++) {
                swap(a[x], a[x + 1]);
            }
        }
        sum = 0;
        iter = 0;
        for (int j = 0; j < k; j++) {
            sum += arr[j][a[j]];
        }
        if(sum > FILTER) {
            const auto z = m.find(num[iter]);
            const auto en = m.end();
            if (z == en) {
                m[num[iter]] = sum;
            } else if (z != en && sum > z->second) {
                z->second = sum;
            }
        }

        for (size_t j = 0; j < SIZE - k; j++) {
            sum -= arr[j][a[iter]];
            sum += arr[j + k][a[iter]];
            iter = (iter + 1) % k;
            if(sum > FILTER) {
                auto z = m.find(num[iter]);
                const auto en = m.end();
                if (z == en) {
                    m[num[iter]] = sum;
                } else if (z != en && sum > z->second) {
                    z->second = sum;
                }
            }
        }
    }
//	for (const auto & p : m) {
//		cout << p.first << " : " << p.second << endl;
//	}
    return m;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    /// посчитаем, сколько ключей было добавлено во все мапы
    size_t total_num_keys = 0;

    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - std::chrono::steady_clock::now()).count();
    for (size_t i = 0; i < NUM_MATRICES; i++) {
        const auto matrix = gen();

        //print_matrix(matrix);
        auto begin = std::chrono::steady_clock::now();

        /// получаем мапу и запоминаем, сколько ключей в нее вошло
        const auto map = f(k, matrix);
        total_num_keys = map.size();

        auto end = std::chrono::steady_clock::now();
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ";
        time += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    }

    std::cout << std::endl;
    std::cout << "Total number of keys: " << total_num_keys << std::endl;
    std::cout << "Average mean time: " << (float)time / (float)NUM_MATRICES << " ms" << std::endl;
    return 0;
}