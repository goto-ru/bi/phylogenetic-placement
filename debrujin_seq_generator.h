#pragma once

#include <cstddef>
#include "debrujin.h"

class debrujin_seq_iterator
{
public:
    using value_type = fxt::ulong;
    /// WARNING: this constructor is supposed to be used only for
    /// the end() iterators
    debrujin_seq_iterator(fxt::ulong value);
    debrujin_seq_iterator(size_t n, size_t k);

    debrujin_seq_iterator(debrujin_seq_iterator&&) = default;
    ~debrujin_seq_iterator() noexcept = default;

    /// WARNING!
    /// this operator is supposed to be used only to compare against
    /// the end() iterator
    bool operator==(const debrujin_seq_iterator& rhs) const noexcept;
    bool operator!=(const debrujin_seq_iterator& rhs) const noexcept;

    debrujin_seq_iterator& operator++();
    value_type operator*() const noexcept;

private:
    fxt::debruijn _fxt_generator;
    fxt::ulong _current_digit;
    size_t _n;
    size_t _k;
};

///
/// Usage: for (auto x : debrujin_seq_generator(n, k)) { ... }
class debrujin_seq_generator
{
public:
    using iterator = debrujin_seq_iterator;

    debrujin_seq_generator(size_t n, size_t k);

    iterator begin();
    iterator end() const;

private:
    size_t _n;
    size_t _k;
};


debrujin_seq_iterator::debrujin_seq_iterator(fxt::ulong value)
        : _fxt_generator{0, 0}, _current_digit{value}
{
}

debrujin_seq_iterator::debrujin_seq_iterator(size_t n, size_t k)
        : _fxt_generator{n, k}, _n{n}, _k{k}
{
    _current_digit = _fxt_generator.first_digit();
}


bool debrujin_seq_iterator::operator==(const debrujin_seq_iterator& rhs) const noexcept
{
    return _current_digit == rhs._current_digit;
}

bool debrujin_seq_iterator::operator!=(const debrujin_seq_iterator& rhs) const noexcept
{
    return !(*this == rhs);
}

debrujin_seq_iterator& debrujin_seq_iterator::operator++()
{
    _current_digit = _fxt_generator.next_digit();
    return *this;
}

debrujin_seq_iterator::value_type debrujin_seq_iterator::operator*() const noexcept
{
    return _current_digit;
}

debrujin_seq_generator::debrujin_seq_generator(size_t n, size_t k)
        : _n{n}, _k{k}
{
}

debrujin_seq_generator::iterator debrujin_seq_generator::begin()
{
    return {_n, _k};
}

debrujin_seq_generator::iterator debrujin_seq_generator::end() const
{
    return {_n};
}
