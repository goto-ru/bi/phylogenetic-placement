#include <bits/stdc++.h>

#include "debrujin_seq_generator.h"

/// Ключи надо хранить в unsigned int 64, отрицательные они все равно быть не могут
using key_type = uint64_t;
using value_type = float;
using map_type =  std::unordered_map<key_type, value_type>;


const size_t SIZE = 1000;
const size_t NUM_MATRICES = 10;
const size_t k = 10;
const value_type FILTER = std::log2(powf(0.25, k));


int binpow (int a, int n) {
    int res = 1;
    while (n) {
        if (n & 1)
            res *= a;
        a *= a;
        n >>= 1;
    }
    return res;
}


class RandomGenerator {
public:
    static std::mt19937 &getMt19937();

private:
    RandomGenerator();
    ~RandomGenerator() {}
    RandomGenerator(RandomGenerator const &) = delete;
    RandomGenerator &operator=(RandomGenerator const &) = delete;

    static RandomGenerator &instance();

    std::mt19937 mMt;
};

RandomGenerator::RandomGenerator() {
    std::random_device rd;
    mMt.seed(42);
}

RandomGenerator &RandomGenerator::instance() {
    static RandomGenerator s;
    return s;
}

std::mt19937 &RandomGenerator::getMt19937() {
    return RandomGenerator::instance().mMt;
}


std::vector<std::vector<float>> gen() {
    std::mt19937 &mt = RandomGenerator::getMt19937();
    std::uniform_real_distribution<float> dist(0.0, 1.0);
    std::vector<std::vector<float>> arr(4, std::vector<float>(SIZE));

    for (size_t i = 0; i < 4; i++) {
        for (size_t j = 0; j < SIZE; j++) {
            arr[i][j] = log2(dist(mt));
        }
    }
    return arr;
}

/// полезная функция для отладки, печатаем коллекцию через пробел. Например работает
/// с std::vector, std::list и подобным (если элементы поддерживают вывод с помощью operator<<)
template<class Container>
void print_container(const Container &container) {
    for (const auto &element: container) {
        std::cout << element << " ";
    }
    std::cout << std::endl;
}

/// печатаем матрицу
template<class Matrix>
void print_matrix(const Matrix &matrix) {
    for (const auto &row: matrix) {
        print_container(row);
    }
}

map_type shift_sum(const std::vector<std::vector<float>> &arr, const std::vector<unsigned short int> &seq) {
    map_type m;

    std::vector<float> sum1(SIZE - k + 1, 0), sum2(SIZE - k + 1, 0);
    key_type num = 0;
    for (size_t i = 0; i < k; ++i) {
        num <<= 2ul;
        num += seq[i];
    }

    float max_value = FILTER;
    for (size_t i = 0; i <= SIZE - k; i++) {
        for (size_t j = 0; j < k; j++) {
            sum1[i] += arr[seq[j]][i + j];
        }

        if (sum1[i] > max_value) {
            max_value = sum1[i];
        }
    }
    /*
    std::cout << std::endl;
    std::cout << "SUM1: ";
    print_container(sum1);
    */

    //std::cout << "Maximum value: " << max_value << std::endl;
    if (max_value > FILTER) {
        m[num] = max_value;

        /*
        std::cout << "Maximum value " << num << " [ ";
        for (size_t x = 0; x < k; ++x) {
            std::cout << seq[x] << " ";
        }
        std::cout << "]: " << max_value << std::endl;
         */
    }

    for (size_t i = 0; i < seq.size() - k; i++) {
        max_value = FILTER;

        /*std::cout << std::endl;
        std::cout << "Letter to remove: " << seq[i] << std::endl;
        std::cout << "Letter to add: " << seq[i + k] << std::endl;

        std::cout << "SUM1: ";
        print_container(sum1);
        */
        for (size_t j = 0; j < SIZE - k; j++) {
            sum2[j + 1] = sum1[j] - arr[seq[i]][j];
            sum2[j + 1] = sum2[j + 1] + arr[seq[i + k]][j + k];

            if (sum2[j + 1] > max_value) {
                max_value = sum2[j + 1];
            }
        }

        sum2[0] = 0;
        for (size_t j = 0; j < k; j++) {
            sum2[0] += arr[seq[i + 1 + j]][j];
        }

        num = 0;
        for (size_t j = 0; j < k; ++j) {
            num <<= 2ul;
            num += seq[i + 1 + j];
        }

        if (sum2[0] > max_value) {
            max_value = sum2[0];
        }

        /*std::cout << "SUM2: ";
        print_container(sum2);
        std::cout << "Maximum value: " << max_value << std::endl;
        */
        if (max_value > FILTER) {
            m[num] = max_value;

            /*
            std::cout << "Maximum value " << num << " [ ";
            for (size_t x = 0; x < k; ++x) {
                std::cout << seq[i + 1 + x] << " ";
            }
            std::cout << "]: " << max_value << std::endl;
            */

        }

        /// Nikolai: познаем как можно копировать массив в плюсах.
        /// На будущее: эту операцию можно исключить, если мы будем подменять массивы один другим
        std::copy(sum2.begin(), sum2.end(), sum1.begin());
    }


    /*
    for (const auto& p : m) {
        std::cout << p.first << " : " << p.second << std::endl;
    }
     */
    return m;
}


int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    std::cout.tie(0);

    /// Nikolai
    /// Не надо генерировать ее каждый раз заново для всех матриц
    auto debruijn_seq_gen = debrujin_seq_generator(4, k);
    std::vector<unsigned short> debruijn_seq;
    for (const auto &value : debruijn_seq_gen) {
        debruijn_seq.push_back(value);
    }

    /// посчитаем, сколько ключей было добавлено во все мапы
    size_t total_num_keys = 0;

    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::steady_clock::now() - std::chrono::steady_clock::now()).count();
    for (size_t i = 0; i < NUM_MATRICES; i++) {
        const auto matrix = gen();

        //print_matrix(matrix);
        auto begin = std::chrono::steady_clock::now();

        /// получаем мапу и запоминаем, сколько ключей в нее вошло
        const auto map = shift_sum(matrix, debruijn_seq);
        //const auto map = my_slow(matrix);
        total_num_keys = map.size();

        auto end = std::chrono::steady_clock::now();
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ";
        time += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    }

    std::cout << std::endl;
    std::cout << "Total number of keys: " << total_num_keys << std::endl;
    std::cout << "Average mean time: " << (float) time / (float) NUM_MATRICES << " ms" << std::endl;
    return 0;
}