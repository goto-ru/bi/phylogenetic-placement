#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <unordered_map>
#include <string>
#include <omp.h>
#include <mkl.h>
#include "debrujin_seq_generator.h"


class RandomGenerator {
public:
	static std::mt19937& getMt19937();

private:
	RandomGenerator();
	~RandomGenerator() {}
	static RandomGenerator& instance();

	RandomGenerator(RandomGenerator const&) = delete;
	RandomGenerator& operator= (RandomGenerator const&) = delete;

	std::mt19937 mMt;
};

RandomGenerator::RandomGenerator() {
	//    std::random_device rd;
	//
	//    if (rd.entropy() != 0) {
	//        std::seed_seq seed{ rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd() };
	//        mMt.seed(seed);
	//    }
	//    else {
	//        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	//        mMt.seed(seed);
	//    }
	mMt.seed(42);
}

RandomGenerator& RandomGenerator::instance() {
	static RandomGenerator s;
	return s;
}

std::mt19937& RandomGenerator::getMt19937() {
	return RandomGenerator::instance().mMt;
}


const size_t SIZE = 1000;
const size_t NUM_MATRICES = 1;
const size_t k = 10;
const float FILTER = std::log2(powf(0.25, k));


std::vector <std::vector <float>> gen() {
	std::mt19937& mt = RandomGenerator::getMt19937();
	std::uniform_real_distribution <float> dist(0.0, 1.0);
	std::vector <std::vector <float>> arr(4, std::vector <float>(SIZE));

	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < SIZE; j++) {
			arr[i][j] = log2(dist(mt));
		}
	}
	return arr;
}


template <class Container>
void print_container(const Container& container) {
	for (const auto& element : container) {
		std::cout << element << " ";
	}
	std::cout << std::endl;
}


template <class Matrix>
void print_matrix(const Matrix& matrix) {
	for (const auto& row : matrix) {
		print_container(row);
	}
}


using key_type = uint64_t;
using value_type = float;
using map_type =  std::unordered_map<key_type, value_type>;

map_type f(size_t k, const std::vector <std::vector <float>>& arr, const std::vector <unsigned short int>& seq) {
	map_type m;


	std::vector <float> sum1(SIZE - k + 1, 0), sum2(SIZE - k + 1, 0);
	key_type num = 0;
	for (size_t i = 0; i < k; ++i) {
		num <<= 2ul;
		num += seq[i];
	}

	float max_value = FILTER;
	for (size_t i = 0; i <= SIZE - k; i++) {
		for (size_t j = 0; j < k; j++) {
			sum1[i] += arr[seq[j]][i + j];
		}

		if (sum1[i] > max_value) {
			max_value = sum1[i];
		}
	}


	if (max_value > FILTER) {
		m[num] = max_value;

		//        std::cout << "Maximum value " << num << " [ ";
		//        for (size_t x = 0; x < k; ++x) {
		//            std::cout << seq[x] << " ";
		//        }
		//        std::cout << "]: " << max_value << std::endl;
	}

	for (size_t i = 0; i < seq.size() - k; i++) {
		max_value = FILTER;
		vsSub(SIZE - k, &sum1[0], &arr[seq[i]][0], &sum2[1]);
		vsAdd(SIZE - k, &sum2[1], &arr[seq[i + k]][k], &sum2[1]);
		/*for (size_t j = 0; j < SIZE - k; j++) {
			sum2[j + 1] = sum1[j] - arr[seq[i]][j];
			sum2[j + 1] = sum2[j + 1] + arr[seq[i + k]][j + k];

			if (sum2[j + 1] > max_value) {
				max_value = sum2[j + 1];
			}
		}*/

		sum2[0] = 0;
		for (size_t j = 0; j < k; j++) {
			sum2[0] += arr[seq[i + 1 + j]][j];
		}

		max_value = sum2[cblas_isamin(SIZE - k + 1, &sum2[0], 1)];

		num = 0;
		for (size_t j = 0; j < k; ++j) {
			num <<= 2ul;
			num += seq[i + 1 + j];
		}

		if (sum2[0] > max_value) {
			max_value = sum2[0];
		}

		if (max_value > FILTER) {
			m[num] = max_value;

			//            std::cout << "Maximum value " << num << " [ ";
			//            for (size_t x = 0; x < k; ++x) {
			//                std::cout << seq[i + 1 + x] << " ";
			//            }
			//            std::cout << "]: " << max_value << std::endl;
		}

		std::copy(sum2.begin(), sum2.end(), sum1.begin());
	}


	    //for (const auto& p : m) {
	    //    std::cout << p.first << " : " << p.second << std::endl;
	    //}

	return m;
}


int main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	std::cout.tie(0);

	auto debruijn_seq_gen = debrujin_seq_generator(4, k);
	std::vector<unsigned short> debruijn_seq;
	for (const auto& value : debruijn_seq_gen) {
		debruijn_seq.push_back(value);
	}


	size_t total_num_keys = 0;

	auto time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - std::chrono::steady_clock::now()).count();
	for (size_t i = 0; i < NUM_MATRICES; i++) {
		const auto matrix = gen();

		//print_matrix(matrix);

		auto begin = std::chrono::steady_clock::now();

		const auto map = f(k, matrix, debruijn_seq);
		total_num_keys += map.size();

		auto end = std::chrono::steady_clock::now();
		time += std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
	}
	std::cout << "Total number of keys: " << total_num_keys << std::endl;
	std::cout << "Total elapsed time: " << time << " ms" << std::endl;

	return 0;
}